const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.autenticarUsuario = async (req, res) => {
     // Revisamos que no haya errores
     const errores = validationResult(req);
     if (!errores.isEmpty()) {
         res.status(400).json({ errores: errores.array() });
     }

    //  Extraemos el mail y el password
    const {email, password} = req.body;

    try {
        // Revisar qeu sea un usuario registrado
        let usuario = await Usuario.findOne({email});
        if(!usuario){
            return res.status(400).json({msg: 'El usuario no existe'});
        }

        // Revisamos que la contraseña es correcta
        const passCorrecto = await bcryptjs.compare(password, usuario.password);
        if(!passCorrecto){
            return res.status(400).json({msg: 'Password incorrecto'});
        }

        // Creamos y firmamos el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // Firmar el token
        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 10800  //Tres horas
        }, (error, token) => {
            if (error) throw error;

            // Mensaje de confirmación
            res.json({token})
        });


    } catch (error) {
        console.log(error)
    }
};


//  Obtiene el usuario autenticado
exports.usuarioAutenticado = async (req, res) => {

    try {
        const usuario = await Usuario.findById(req.usuario.id).select('-password');
        res.json({usuario});
    } catch (error) {
        console.log(error)
        res.status(500).json({msg: 'Hubo un error'})
    }
}