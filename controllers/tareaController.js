const Tarea = require('../models/Tarea');
const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');

// Crear una tarea 
exports.crearTarea = async (req, res) => {

    // Revisamos que no haya errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        res.status(400).json({ errores: errores.array() });
    }


    try {

        // Extraemos el proyecto
        const { proyecto } = req.body;

        // Comprobamos que el proyecto exista
        const existeProyecto = await Proyecto.findById(proyecto);
        if (!existeProyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' })
        }

        // Revisamos si el proyecto actual pertenece al usuario autenticado
        if (existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado' })
        }

        // Creamos la tarea
        const tarea = new Tarea(req.body);
        await tarea.save();
        console.log("CREAR TAREA")
        res.json({ tarea });

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Hubo un error' });
    }
};


// Obtener las tareas por proyecto
exports.obtenerTareas = async (req, res) => {

    try {
        // Extraemos el proyecto
        const { proyecto } = req.query;

        // Comprobamos que el proyecto exista
        const existeProyecto = await Proyecto.findById(proyecto);
        if (!existeProyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado' })
        }

        // Revisamos si el proyecto actual pertenece al usuario autenticado
        if (existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado' })
        }

        // Obtener las tareas por proyecto
        const tareas = await Tarea.find({ proyecto }).sort({creado: -1});
        console.log('----------------- OBTENER Tareas API -------------------');
        console.log(tareas);
        console.log('----------------------- FIN ------------------------')
        res.json({ tareas });

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Hubo un error' })
    }
};


// Actualizar tarea
exports.actualizarTarea = async (req, res) => {

    try {
        // Extraemos el proyecto | nombre y estado de la tarea
        const { proyecto, nombre, estado } = req.body;

        // Comprobamos que la tarea exista
        let tarea = await Tarea.findById(req.params.id);

        if(!tarea){
            return res.status(404).json({msg: 'Tarea no encontrada'})
        }

        // Comprobamos que el proyecto exista
        const existeProyecto = await Proyecto.findById(proyecto);

        // Revisamos si el proyecto actual pertenece al usuario autenticado
        if (existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado' })
        }

        // Crear un objeto con la nueva información
        const nuevaTarea = {};
        nuevaTarea.nombre = nombre;
        nuevaTarea.estado = estado;
        
        // Guardamos la tarea
        tarea = await Tarea.findOneAndUpdate({_id: req.params.id}, nuevaTarea, {new: true})
        res.json({tarea})

    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Hubo un error' });
    }
};

// Eliminamos tareas
exports.eliminarTarea = async (req, res) => {
    try {
        // Extraemos el proyecto | nombre y estado de la tarea
        const { proyecto } = req.query;

        // Comprobamos que la tarea exista
        let tarea = await Tarea.findById(req.params.id);

        if(!tarea){
            return res.status(404).json({msg: 'Tarea no encontrada'})
        }

        // Comprobamos que el proyecto exista
        const existeProyecto = await Proyecto.findById(proyecto);

        // Revisamos si el proyecto actual pertenece al usuario autenticado
        if (existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado' })
        }

        // Eliminamos la tarea
        await Tarea.findOneAndRemove({_id: req.params.id});
        res.json({msg: 'La tarea ha sido eliminada'});

    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error' );
    }
}