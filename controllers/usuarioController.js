const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.crearUsuario = async (req, res) => {

    // Revisamos que no haya errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        res.status(400).json({ errores: errores.array() });
    }

    // Extraer email y password
    const { email, password } = req.body;

    try {

        let usuario = await Usuario.findOne({ email });
        if (usuario) {
            return res.status(400).json({ msg: 'El mail ya está registrado' });
        }

        // Creamos al usuario
        usuario = new Usuario(req.body);

        // Hasheamos la contraseña
        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash(password, salt);

        // Guardamos el nuevo usuario
        await usuario.save();

        // Creamos y firmamos el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // Firmar el token
        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 10800  //Tres horas
        }, (error, token) => {
            if (error) throw error;

            // Mensaje de confirmación
            res.json({token})
        });


    } catch (error) {
        console.log(error);
        res.status(400).send('Hubo un error');
    }
}