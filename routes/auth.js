// Rutas para autenticar usuarios
const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const { check } = require('express-validator');
const auth = require('../middleware/auth');

// Inicio sesión
// api/auth
router.post('/', 

    // [
    //     check('email','Introduce un email válido').isEmail(),
    //     check('password', "la contraseña debe de tener al menos 4 carácteres").isLength(4)
    // ],
    authController.autenticarUsuario
);

// Obtiene el usuario autenticado
router.get('/',
    auth,
    authController.usuarioAutenticado
);

module.exports = router;