// Rutas para crear usuarios
const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarioController');
const {check } = require('express-validator');

// Crea un usuario
// api/usuarios
router.post('/', 

    [
        check('nombre','El nombre es obligatorio').not().isEmpty(),
        check('email','Introduce un email válido').isEmail(),
        check('password', "la contraseña debe de tener al menos 4 carácteres").isLength(4)
    ],
   usuarioController.crearUsuario
);

module.exports = router;